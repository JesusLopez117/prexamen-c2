//Se encontraran todas las rutas
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

//Ruta para la pagina formulario
router.get('/', (req, res) => {
    const params = {
        numBoleto: req.query.numBoleto,
        destino: req.query.destino,
        nomCliente: req.query.nomCliente,
        añosCum: parseFloat(req.query.añosCum),
        tipViaje: parseInt(req.query.tipViaje),
        precio: parseFloat(req.query.precio)
    };

    res.render('formulario.html', params);
});

router.post('/', (req, res) => {
    const params = {
        numBoleto: req.body.numBoleto,
        destino: req.body.destino,
        nomCliente: req.body.nomCliente,
        añosCum: parseFloat(req.body.añosCum),
        tipViaje: parseInt(req.body.tipViaje),
        precio: parseFloat(req.body.precio)
    };

    res.render('formulario.html', params);
});

module.exports = router;