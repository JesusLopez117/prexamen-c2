//Comenzar un servidor

//Constantes necesarias
const http = require('http');
const express = require('express');
const bodyparser = require('body-parser'); //Libreria para trabajar el método get y post
const misRutas = require('./router/index');
const path = require('path');

const app = express();

app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public/"));
app.use(bodyparser.urlencoded({extended:true}));

app.engine('html', require('ejs').renderFile);
app.use(misRutas);  //Referencia para que vaya a mis rutas y sea utilizado.

//La pagína de error va al final de los get/post
app.use((req,res,next) =>{
    res.status(404).sendFile(__dirname + '/public/error.html');
});

//Inidicar el puerto por cual va a salir
const puerto = 3000;
app.listen(puerto, () => {
    console.log(`Iniciando por el puerto ${puerto}`);
});